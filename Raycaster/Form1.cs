﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Raycaster
{
    public partial class Form1 : Form
    {
        private Bitmap _backbuffer;
        private int _zoom = 3;
        private MiniMap _map = new MiniMap();
        private Level _level = new Level();
        private Player _Player = new Player();

        public Form1()
        {
            InitializeComponent();
            _backbuffer = new Bitmap(100,100, PixelFormat.Format24bppRgb);
            var  lockData = _backbuffer.LockBits(new Rectangle(0,0, _backbuffer.Size.Width, _backbuffer.Size.Height),
                ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            Draw(lockData);
            _backbuffer.UnlockBits(lockData);
        }

        private static unsafe void Draw(BitmapData lockData)
        {
            byte* b = (byte*) lockData.Scan0;
            for (int j = 0; j < lockData.Height; j++)
            {
                for (int i = 0; i < lockData.Width; i++)
                {
                    b[(j * lockData.Width + i)*3+0] = (byte) (i % 256);
                    b[(j * lockData.Width + i)*3+1] = (byte) (j % 256);
                    b[(j * lockData.Width + i)*3+2] = (byte) (0);
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            this.DoubleBuffered = true;
            base.OnPaint(e);
            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor; 
            e.Graphics.DrawImage(_backbuffer, 0,0, _backbuffer.Width * _zoom, _backbuffer.Height * _zoom);

            _map.OnPaint(e, _level, _Player);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            var speed = 0.1f;
            switch (e.KeyCode)
            {
                case Keys.Up:
                    _Player.Position += _Player.Forward * speed;
                    break;
                case Keys.Down:
                    _Player.Position -= _Player.Forward * speed;
                    break;
                case Keys.Right:
                    _Player.Angle += 0.098174770424681f;
                    //_Player.Position += _Player.Right;
                    break;
                case Keys.Left:
                    _Player.Angle -= 0.098174770424681f;
                    //_Player.Position -= _Player.Right;
                    break;
            }
            Invalidate();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            _zoom += Math.Sign(e.Delta);
            _zoom = _zoom < 1 ? 1 : _zoom;
            this.Invalidate();
        }
    }

    public class Player
    {
        public Vector2 Position;

        // Rad
        public float Angle;
        public Vector2 Forward => new Vector2((float) Math.Cos(Angle), (float) Math.Sin(Angle));
        public Vector2 Right => new Vector2(-Forward.Y, Forward.X);
}
    public class Level
    {
        public int Width = 4;
        public int Height = 4;
        private int[][] _data =
        {
            new[]{1,1,0,0 },
            new[]{1,0,1,0 },
            new[]{1,0,1,0 },
            new[]{1,0,1,1 },
        };
        public int this[int x, int y] => _data[y][x];
    }

    public class MiniMap
    {
        Rectangle size = new Rectangle(0,0, 100, 100);
        public void OnPaint(PaintEventArgs e, Level level, Player player)
        {
            e.Graphics.FillRectangle(Brushes.White, size);
            e.Graphics.DrawRectangle(Pens.Black, size);
            int blockSize = Math.Min(size.Width / level.Width, size.Height / level.Height);
            for (int i = 0; i < level.Width; i++)
            {
                for (int j = 0; j < level.Height; j++)
                {
                    if (level[i, j] == 1)
                    {
                        var b = Brushes.CadetBlue;
                        e.Graphics.FillRectangle(b, i * blockSize, j * blockSize, blockSize, blockSize);
                    }

                    e.Graphics.DrawRectangle(Pens.Gray, i*blockSize, j*blockSize, blockSize, blockSize);
                }
            }

            e.Graphics.DrawEllipse(Pens.DeepSkyBlue, player.Position.X * blockSize - 4, player.Position.Y * blockSize - 4, 8, 8);
            Vector2 vector2 = (player.Position * blockSize);
            e.Graphics.DrawLine(Pens.Red, vector2.ToPointF(), (vector2 + player.Forward * 10).ToPointF());
        }
    }

    public static class MathExtensions
    {
        public static PointF ToPointF(this Vector2 v)
        {
            return new PointF(v.X, v.Y);
        }
    }
}
